import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableComponent } from './component/table/table.component';
import { PaginatorComponent } from './component/shared/paginator/paginator.component';

import { GetTitlePipe } from './pipes/get-title.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    GetTitlePipe,
    PaginatorComponent,
    GetTitlePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

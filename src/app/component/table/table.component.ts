import { Component, OnInit } from '@angular/core';
import { TableService } from '../../services/table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  isLoading: boolean;
  tableData: any[];
  fromRecord: number;
  toRecord: number;
  totalRecords: number;
  numberOfRecords: number;
  showOptions: number[];

  constructor(public tService: TableService) {
    this.numberOfRecords = 10;
    this.toRecord = 10;
    this.fromRecord = 0;
    this.isLoading = true;
    this.showOptions = tService.getShowOptions();
  }

  ngOnInit() {
    this.tService.loadTableInfo().subscribe((data) => {
      this.tableData = data;
      this.totalRecords = data.length;
      this.numberOfRecords = data.length;
      this.toRecord = data.length;
      this.isLoading = false;
    });
  }

  displayRecordsFrom(nRecords: number, index: number) {
    if (!isNaN(nRecords) && !isNaN(index)) {
      this.numberOfRecords = nRecords;
      // Remember js is based 0
      if (index > 0) {
        index -= 1;
      }
      this.fromRecord = index * nRecords;
      this.toRecord = this.fromRecord + Number(nRecords);
    } else {
      if (isNaN(nRecords) && !isNaN(index)) {
        this.fromRecord = 0;
        this.toRecord = this.tableData.length;
        this.numberOfRecords = this.tableData.length;
      } else {
        console.error('None of the values introduced are a number.');
      }
    }
  }

  submitInfo(id: any, status: any) {
    this.tService.postTableData(id, status);
  }


}

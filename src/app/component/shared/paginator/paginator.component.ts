import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnChanges {

  @Input() itemsDisplayed: any;
  @Input() totalRecords: number;
  @Output() idClicked: EventEmitter<number> = new EventEmitter();
  indexButtons: number[];

  constructor() {
    this.indexButtons = [];
  }

  ngOnChanges() {
    this.setPaginator();
  }

  indexClicked(id: number) {
    this.idClicked.emit(id);
  }

  setPaginator() {
    const NUMBEROFPAGES = Math.floor(this.totalRecords / this.itemsDisplayed);
    this.indexButtons = [];
    for (let i = 1; i <= NUMBEROFPAGES; i++) {
      this.indexButtons.push(i);
    }
  }
}

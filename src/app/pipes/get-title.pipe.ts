import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getTitle'
})
export class GetTitlePipe implements PipeTransform {

  transform(value: any): any {
    const TITLES: string[] = [];
    for (const NAMES in value) {
      if (NAMES) {
        TITLES.push(NAMES);
      }
    }

    return TITLES;
  }

}

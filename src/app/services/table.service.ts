import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  tableItems: any[];
  showOptions: any[];

  constructor( private http: HttpClient) {
    this.tableItems = [];
    this.showOptions = ['All', 10, 15, 20, 30, 50];
  }

  loadTableInfo() {
    return this.http.get('./assets/json/sample_data.json').pipe(
      map( (data: any) => {
        for (const key in data) {
          if (key) {
            this.tableItems.push(data[key]);
          }
        }
        return this.tableItems;
      })
    );
  }

  getShowOptions() {
    return this.showOptions;
  }

  postTableData(id: any, status: any) {
    const TABLEDATA = {
      id,
      status
    };
    const BODY: string = JSON.stringify(TABLEDATA);
    const HEADER: HttpHeaders = new HttpHeaders({
      'content-type': 'applicaion/json'
    });
    this.http.post('/api/submit', BODY, {headers: HEADER} );
  }

}
